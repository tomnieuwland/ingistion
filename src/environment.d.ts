declare global {
  namespace NodeJS {
    interface ProcessEnv {
      REDIS_HOST: string;
      REDIS_PORT: number;
      GITHUB_ACCESS_TOKEN: string;
    }
  }
}

export {};
