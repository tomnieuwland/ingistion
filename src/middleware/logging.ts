import { NextFunction, Request, Response } from 'express';
import { logger } from '../logger';

/**
 * Middleware for logging all requests to debug transport
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export function loggingMiddleware(req: Request, res: Response, next: NextFunction) {
  logger.debug(`Recieved ${req.method} ${req.url}`);
  next();
}
