import { processJob } from './bull/processors/processors';
import { GIST_INGESTION_QUEUE } from './bull/queues';
import { logger } from './logger';

GIST_INGESTION_QUEUE.process(async (job) => {
  logger.info(`Handling job of type ${job.data.type}`);
  processJob(job);
});

logger.info(`Worker has started and is waiting for work`);
