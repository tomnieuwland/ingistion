import Bull from 'bull';

export const GIST_INGESTION = 'GIST_INGESTION';
export const GIST_INGESTION_QUEUE = new Bull(GIST_INGESTION, {
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
  }
});
