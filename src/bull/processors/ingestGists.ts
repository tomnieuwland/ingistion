import { PrismaClient } from '@prisma/client';
import axios from 'axios';
import { Job } from 'bull';
import { GITHUB_GRAPHQL_ENDPOINT } from '../../constants';
import { logger } from '../../logger';

const prisma = new PrismaClient();

/**
 * Constructs a GraphQL query and requests it from the Github GraphQL API
 *
 * @param {string} username
 * @param {number} pageSize
 * @param {(String | null)} cursor
 * @return {*}  {Promise<IngestGistsData>}
 */
export async function fetchGists(username: string, pageSize: number, cursor: String | null): Promise<IngestGistsData> {
  let gistFilters: string;

  if (cursor) {
    // If we have a cursor, we want to continue from there for pagination of results
    gistFilters = `first: ${pageSize}, after: "${cursor}"`;
  } else {
    gistFilters = `first: ${pageSize}`;
  }

  let response = axios.request({
    url: GITHUB_GRAPHQL_ENDPOINT,
    method: 'POST',
    headers: {
      Authorization: `bearer ${process.env.GITHUB_ACCESS_TOKEN}`,
      Accept: 'application/json'
    },
    data: JSON.stringify({
      query: `query {
        viewer {
          login
        }
        user(login: "${username}") {
          id
          gists(${gistFilters}) {
            pageInfo {
              hasNextPage
              endCursor
            }
            edges {
              node {
                id
                createdAt
                files {
                  name
                  text
                }
                url
                updatedAt
              }
            }
          }
        }
      }`
    })
  });

  return (await response).data;
}

/**
 * Fetches a user's gists for Github and then ingests them into the postgres database
 *
 * @export
 * @param {Job} job
 */
export async function ingestGists(job: Job) {
  const user = job.data.payload.username;
  let cursor: string | null = null;
  let hasNextPage = false;

  logger.info(`Ingesting gists for user ${user}`);

  do {
    let obj: IngestGistsData = await fetchGists(user, 10, cursor);
    let gists = obj.data.user.gists;

    hasNextPage = gists.pageInfo.hasNextPage;
    cursor = gists.pageInfo.endCursor;

    // Each edge.node is what we would refer to as a "gist"
    await Promise.all(
      gists.edges.map(async (edge) => {
        return syncNode(user, edge.node);
      })
    );
  } while (hasNextPage);

  logger.debug(`Updating info for user ${user}`);
  await prisma.user.update({
    where: {
      id: user
    },
    data: {
      lastRetrieved: new Date().toISOString()
    }
  });
  logger.debug(`Successfully ingested gists for user ${user}`);
}

/**
 * Imports a single gist Node into the database
 *
 * @param {string} user
 * @param {GistNode} node
 * @return {*} 
 */
async function syncNode(user: string, node: GistNode) {
  logger.debug(`Attemping to import gist ${node.id}`);
  let gist = await prisma.gist.findFirst({
    where: {
      id: node.id
    }
  });

  // The gist already exists in our database
  if (gist) {
    // The gist hasn't been updated since we last checked
    if (gist.updatedAt.toISOString() == node.updatedAt) {
      logger.debug(`Gist ${node.id} is already up to date`);
      return;
    }
  }

  // The gist either doesn't exist or is out of date
  logger.debug(`Upserting ${node.id}`);
  await prisma.gist.upsert({
    where: {
      id: node.id
    },
    update: {
      id: node.id,
      url: node.url,
      updatedAt: node.updatedAt,
      userId: user
    },
    create: {
      id: node.id,
      url: node.url,
      createdAt: node.createdAt,
      updatedAt: node.updatedAt,
      userId: user
    }
  });

  await syncFiles(node);
}

/**
 * Imports all the files attached to a Gist node
 *
 * @param {GistNode} node
 */
async function syncFiles(node: GistNode) {
  // Delete any files (if they exist for this gist) so that they can be re-syncronised
  // Github doesn't store updates at the file level, so if a gist has been updated we
  // have to assume that all files are out of date
  logger.debug(`Deleting any files that are associated with gist ${node.id}`);
  await prisma.file.deleteMany({
    where: {
      gistId: node.id
    }
  });

  // Recreate any files that are attached to this gist
  logger.debug(`Creating files for gist ${node.id}`);
  await prisma.file.createMany({
    data: node.files.map((file) => {
      /* Splats the object down to
        {
          name: <name>,
          text: <contents>,
          gistId: <parent gist id>
        }
      */
      return {
        ...file,
        gistId: node.id
      };
    })
  });
}
