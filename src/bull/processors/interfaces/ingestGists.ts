interface File {
  readonly name: string;
  readonly text: string;
}

interface GistNode {
  readonly id: string;
  readonly createdAt: string;
  readonly files: File[];
  readonly url: string;
  readonly updatedAt: string;
}

interface Edge {
  readonly node: GistNode;
}

interface PageInfo {
  readonly hasNextPage: boolean;
  readonly endCursor: string;
}

interface Gists {
  readonly pageInfo: PageInfo;
  readonly edges: Edge[];
}

interface User {
  readonly gists: Gists;
}

interface Viewer {
  readonly login: string;
}

interface Data {
  readonly viewer: Viewer;
  readonly user: User;
}

interface IngestGistsData {
  readonly data: Data;
}
