import { Job } from 'bull';
import { logger } from '../../logger';
import { ingestGists } from './ingestGists';

const PROCESSOR_REGISTRY: Record<string, Function> = {
  ingestGists: ingestGists
};

/**
 * Attempts to process a bull job using jobs registered in the PROCESSOR_REGISTRY
 *
 * @export
 * @param {Job} job
 */
export function processJob(job: Job) {
  let processor = PROCESSOR_REGISTRY[job.data.job];

  if (!processor) {
    logger.error(`Could not find a registered processor for job ${job.data.job}`);
    job.moveToFailed({ message: `Could not find a registered processor for job ${job.data.job}` });
  } else {
    processor(job);
  }
}
