import express from 'express';
import Arena from 'bull-arena';

import { logger } from './logger';
import { loggingMiddleware } from './middleware/logging';
import healthCheckRouter from './routes/healthcheck';
import usersRouter from './routes/users/users';
import { GIST_INGESTION } from './bull/queues';
import Bull from 'bull';

const app = express();
const port = process.env.PORT || 3000;

// Bull Arena
app.use(
  '/arena',
  Arena(
    {
      Bull,
      queues: [
        {
          name: GIST_INGESTION,
          hostId: 'worker',
          redis: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT
          }
        }
      ]
    },
    {
      basePath: '/',
      disableListen: true
    }
  )
);

// Middleware
app.use(loggingMiddleware);

// Application Routes
app.use('/healthcheck', healthCheckRouter);
app.use('/users', usersRouter);

app.listen(port, () => {
  logger.info(`Listening on port ${port}`);
  logger.debug(`Debug env vars`);
  logger.debug(
    JSON.stringify({
      REDIS_HOST: process.env.REDIS_HOST,
      REDIS_PORT: process.env.REDIS_PORT,
      PORT: process.env.PORT,
      GITHUB_ACCESS_TOKEN: process.env.GITHUB_ACCESS_TOKEN
    })
  );
});
