import { Prisma, PrismaClient } from '@prisma/client';
import { Router } from 'express';
import { GIST_INGESTION_QUEUE } from '../../bull/queues';
import { logger } from '../../logger';
import gistRouter from './gists/gists';

const usersRouter = Router({ mergeParams: true });
const prisma = new PrismaClient();

usersRouter.use('/:userID/gists', gistRouter);

// List all users
usersRouter.get('/', async (req, res) => {
  const users = await prisma.user.findMany();

  res.json(users);
});

// Get certain user
usersRouter.get('/:id', async (req, res) => {
  const user = await prisma.user.findFirst({
    where: {
      id: req.params.id
    }
  });

  if (user) {
    res.json(user);
  } else {
    res.status(404).send();
  }
});

// Subscribe to a user's gists
usersRouter.post('/:id', async (req, res) => {
  logger.debug(`Attempting to create user ${req.params.id}`);
  try {
    let user = await prisma.user.create({
      data: {
        id: req.params.id
      }
    });

    logger.debug(`Adding ingestion job for user ${user.id} to queue`);
    GIST_INGESTION_QUEUE.add(
      {
        job: 'ingestGists',
        payload: {
          username: user.id
        }
      },
      {
        jobId: user.id,
        repeat: {
          every: 60000
        }
      }
    );

    res.status(201).send();
  } catch (e) {
    if (e instanceof Prisma.PrismaClientKnownRequestError) {
      if (e.code === 'P2002') {
        res.status(400).json({
          message: 'User already exists'
        });
        return;
      }
    } else {
      throw e;
    }
  }
});

// Unsubscribe from a user's Gists and delete them
usersRouter.delete('/:id', async (req, res) => {
  try {
    await prisma.user.delete({
      where: {
        id: req.params.id
      }
    });

    const repeatableJobs = await GIST_INGESTION_QUEUE.getRepeatableJobs()
    const job = repeatableJobs.find(job => job.id === req.params.id)
    if (job){
      await GIST_INGESTION_QUEUE.removeRepeatableByKey(job.key)
      logger.info(`Removed sync job for user ${req.params.id} from queue`)
    }    

    res.status(200).send();
  } catch (e) {
    if (e instanceof Prisma.PrismaClientKnownRequestError) {
      if (e.code === 'P2025') {
        res.status(400).send({
          message: 'User does not exist'
        });
        return;
      }
    }
  }
});

export default usersRouter;
