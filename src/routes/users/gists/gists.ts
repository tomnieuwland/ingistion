import { PrismaClient } from '@prisma/client';
import { Request, Router } from 'express';

const gistRouter = Router();
const prisma = new PrismaClient();

gistRouter.get('/', async (req: Request<{ userID: string }>, res) => {
  const gists = await prisma.gist.findMany({
    where: {
      userId: req.params.userID
    }
  });

  res.json(gists);
});

// Get certain gist
gistRouter.get('/:gistID', async (req: Request<{ userID: string; gistID: string }>, res) => {
  const gist = await prisma.gist.findFirst({
    where: {
      id: req.params.gistID,
      userId: req.params.userID
    },
    include: {
      files: true
    }
  });

  if (gist) {
    res.json(gist);
  } else {
    res.status(404).send();
  }
});

export default gistRouter;
