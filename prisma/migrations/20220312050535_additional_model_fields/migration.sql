/*
  Warnings:

  - Added the required column `name` to the `File` table without a default value. This is not possible if the table is not empty.
  - Added the required column `text` to the `File` table without a default value. This is not possible if the table is not empty.
  - Added the required column `createdAt` to the `Gist` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updatedAt` to the `Gist` table without a default value. This is not possible if the table is not empty.
  - Added the required column `url` to the `Gist` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "File" ADD COLUMN     "name" TEXT NOT NULL,
ADD COLUMN     "text" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Gist" ADD COLUMN     "createdAt" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "updatedAt" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "url" TEXT NOT NULL;
