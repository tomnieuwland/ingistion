-- DropForeignKey
ALTER TABLE "File" DROP CONSTRAINT "File_gistId_fkey";

-- DropForeignKey
ALTER TABLE "Gist" DROP CONSTRAINT "Gist_userId_fkey";

-- AddForeignKey
ALTER TABLE "Gist" ADD CONSTRAINT "Gist_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "File" ADD CONSTRAINT "File_gistId_fkey" FOREIGN KEY ("gistId") REFERENCES "Gist"("id") ON DELETE CASCADE ON UPDATE CASCADE;
