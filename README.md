# Ingistion

Github Gist ingestion service with synchronization capabilities 

## Requirements
- docker
- docker-compose
- github access token for the graphql endpoint (see: https://docs.github.com/en/graphql/guides/forming-calls-with-graphql#authenticating-with-graphql)
- something that can send http requests (there is an attached Postman collection)

## Setting up
- Copy the `.env.example` file and call the copy `.env`
- Add your github access token to the newly created `.env` file
- Run `docker-compose up -d`
- OPTIONAL: Run `docker-compose up worker server` to see outputs from the services
- Use API as you would expect to use any HTTP REST API
- HEADS UP: It can take 60 seconds for the gists to import for the first time :)
## Cleaning up
- `docker-compose down -v` (warning, this will delete all data the app has created! If you just want to bring the app down, remove the `-v` flag)

## Endpoints
- `GET /healthcheck` - Returns 200 if app is running
- `GET /users/` - Returns list of users
- `POST /users/<github_username>/` - Adds user to service
- `DELETE /users/<github_username>/` - Removes user from service
- `GET /users/<github_username>/gists/` - Lists all users gists
- `GET /users/<github_username>/gists/<gist_id>/` - Gets a unique gist and all files associated with it

## Exploration
While the application is running
- You can connect to the postgres database via `postgresql://postgres:password@localhost:5433/ingistion?schema=public`
- You can connect to redis via `localhost:6380`
- You can look at the workers and queues via Bull Arena at `http://localhost:3000/arena/`
- You can use the provided Postman collection to interact with the API

## Tech Used
- Prisma + Postgres for relational data storage and persistance
- Redis + Bull for asyncronous job queues
- Express as a HTTP framework
- Typescript

## Whats missing?
Unfortunately I tried to timebox myself to 6 hours so I had to cut a few things out
- Unit test suite. Probably the most important thing missing. I would have used mocha and chai to test the code that is here, but I figured it was more important to have a working solution than a half finished/broken one with a testing suite
- Better spreading of jobs over worker(s). Currently jobs just repeat every 60 seconds, which means all the jobs can pile up around the same time depending on when the user is first added to the queue, meaning you can be rate limited by github if too many users are being sync'd around the same time
- An actual graphql client. I ran out of time to learn a JS graphql client that would give me something nicer than firing a query as a big hunk of text
- Turning off debug mode logging in an intelligent way